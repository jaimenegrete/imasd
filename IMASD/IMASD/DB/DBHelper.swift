//
//  DBHelper.swift
//  IMASD
//
//  Created by Jaime on 25/11/20.
//

import UIKit
import SQLite3

class DBHelper
{
    init()
    {
        db = openDatabase()
        createTable()
    }

    let dbPath: String = "myDb.sqlite"
    var db:OpaquePointer?

    func openDatabase() -> OpaquePointer?
    {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(dbPath)
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK
        {
            print("error opening database")
            return nil
        }
        else
        {
            print("Successfully opened connection to database at \(dbPath)")
            return db
        }
    }
    
    func createTable() {
        let createTableString = "CREATE TABLE IF NOT EXISTS Person(Id INTEGER PRIMARY KEY,area TEXT, name TEXT, lastName TEXT, lastName2 TEXT, phone TEXT, birthdate TEXT, email TEXT );"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("person table created.")
            } else {
                print("person table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    
    func insert(person: Person)
    {
        let persons = getAll()
        for p in persons
        {
            if p.id == person.id
            {
                return
            }
        }
        let insertStatementString = "INSERT INTO Person ( area, name, lastName, lastName2, phone, birthdate, email) VALUES (?,?,?,?,?,?,?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            //sqlite3_bind_int(insertStatement, 1, Int32(person.id))
            sqlite3_bind_text(insertStatement, 1, (person.area as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, (person.name as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 3, (person.lastName as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 4, (person.lastName2 as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 5, (person.phone as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 6, (person.birthdate as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 7, (person.email as NSString).utf8String, -1, nil)

            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    func getAll() -> [Person] {
        let queryStatementString = "SELECT * FROM Person;"
        var queryStatement: OpaquePointer? = nil
        var persons : [Person] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let id = sqlite3_column_int(queryStatement, 0)
                let area = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let name = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let lastName = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                let lastName2 = String(describing: String(cString: sqlite3_column_text(queryStatement, 4)))
                let phone = String(describing: String(cString: sqlite3_column_text(queryStatement, 5)))
                let birthdate = String(describing: String(cString: sqlite3_column_text(queryStatement, 6)))
                let email = String(describing: String(cString: sqlite3_column_text(queryStatement, 7)))

                persons.append(
                    Person(id: Int(id),
                           area: area,
                           name: name,
                           lastName: lastName,
                           lastName2: lastName2,
                           phone: phone,
                           birthdate: birthdate,
                           email: email))
                
                print("\(name) with id \(id) found")
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return persons
    }
    
    func deleteByID(id:Int) {
        let deleteStatementStirng = "DELETE FROM Person WHERE Id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(id))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    func getByID(id:Int) {
        let searchStatementString = "SELECT * FROM Person WHERE Id = ?;"
        var searchStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, searchStatementString, -1, &searchStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(searchStatement, 1, Int32(id))
            if sqlite3_step(searchStatement) == SQLITE_DONE {
                print("person not found")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(searchStatement)
    }
    
    func update(person: Person) {
        let updateStatementString = "UPDATE Person SET area = ?, name = ?, lastName = ?, lastName2 = ?, phone = ?, birthdate = ?, email = ? WHERE Id = ?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(updateStatement, 8, Int32(person.id))
            sqlite3_bind_text(updateStatement, 1, (person.area as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 2, (person.name as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 3, (person.lastName as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 4, (person.lastName2 as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 5, (person.phone as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 6, (person.birthdate as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 7, (person.email as NSString).utf8String, -1, nil)
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("person updated")
            } else {
                print("Could update person.")
            }
        } else {
            print("Update statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
    }
    
}
