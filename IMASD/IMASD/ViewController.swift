//
//  ViewController.swift
//  IMASD
//
//  Created by Jaime on 25/11/20.
//

import UIKit
import SQLite3

class ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var syncButton: UIButton!
    
    let dbPath: String = "myDb.sqlite"
    let db: DBHelper = DBHelper()
    var persons: [Person] = []
    var personSelected: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPersons()
    }
    
    func loadPersons(){
        persons = db.getAll()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = persons[indexPath.row].name + " " + persons[indexPath.row].lastName
        (cell.viewWithTag(2) as! UILabel).text = persons[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        personSelected = persons[indexPath.row]
        performSegue(withIdentifier: "toNewItem", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toNewItem" {
            let newItemViewController = segue.destination as! NewItemViewController
            newItemViewController.delegate = self
            newItemViewController.person = personSelected
            personSelected = nil
        }
        //segue.destination.modalPresentationStyle = .fullScreen
    }

    func openDatabase() -> OpaquePointer? {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    .appendingPathComponent(dbPath)
        var db: OpaquePointer? = nil
        
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK {
            print("Successfully opened connection to database at \(fileURL.path)")
            return db
        } else {
            print("Unable to open database.")
            return nil
        }
    }
    
    @IBAction func syncToCloud(_ sender: Any){
        let url = URL(string: "https://jj-mysql.herokuapp.com/sync")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        var items : [Any] = []
        
        for person in persons {
            items.append([
                "id" : person.id.description,
                "area": person.area,
                "name": person.name,
                "lastName" : person.lastName,
                "lastName2" : person.lastName2,
                "phone" : person.phone,
                "birthdate": person.birthdate,
                "email": person.email
            ])
            
        }
        
        let parameters: [String: Any] = [
            "items": items
        ]
        let bodyData = try? JSONSerialization.data(
            withJSONObject: parameters,
            options: []
        )
        request.httpBody = bodyData
        self.syncButton.isHidden = true
        self.loadingIndicator.startAnimating()
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            DispatchQueue.main.async { // Main thread
                self.syncButton.isHidden = false
                self.loadingIndicator.stopAnimating()
                
            }
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }

            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            //let responseString = String(data: data, encoding: .utf8)
            //print("responseString = \(responseString)")
        }

        task.resume()
    }
}

extension ViewController: NewItemDelegate {
    
    func itemSaved() {
        loadPersons()
    }
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}
