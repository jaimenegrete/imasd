//
//  Person.swift
//  IMASD
//
//  Created by Jaime on 25/11/20.
//

import UIKit

class Person
{
    
    var id: Int = 0
    var area: String = ""
    var name: String = ""
    var lastName: String = ""
    var lastName2: String = ""
    var phone: String = ""
    var birthdate: String = ""
    var email: String = ""

    init(id: Int, area: String, name: String, lastName: String, lastName2: String, phone: String, birthdate: String, email: String)
    {
        self.id = id
        self.area = area
        self.name = name
        self.lastName = lastName
        self.lastName2 = lastName2
        self.phone = phone
        self.birthdate = birthdate
        self.email = email

    }
    
}
