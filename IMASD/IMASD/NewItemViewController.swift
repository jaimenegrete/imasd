//
//  NewItemViewController.swift
//  IMASD
//
//  Created by Jaime on 25/11/20.
//

import UIKit

protocol NewItemDelegate {
    func itemSaved()
}

class NewItemViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var lastName2TextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var birthdateTextField: UITextField!
    @IBOutlet weak var areaSegmentedControl: UISegmentedControl!
    
    var person : Person?
    var birthdate = ""
    var area = "Software"
    var delegate : NewItemDelegate?
        
    let borderRadius = CGFloat(8)
    let borderColor = UIColor(named: "AccentColor")?.cgColor
    let borderWith = CGFloat(1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.set(textField: nameTextField, text: person?.name ?? "")
        self.set(textField: lastNameTextField, text: person?.lastName ?? "")
        self.set(textField: lastName2TextField, text: person?.lastName2 ?? "")
        self.set(textField: phoneTextField, text: person?.phone ?? "")
        self.set(textField: emailTextField, text: person?.email ?? "")
        self.set(textField: birthdateTextField, text: person?.birthdate ?? "")
        self.setDatePickerFor(textField: birthdateTextField)
        
        if person != nil {
            deleteButton.isHidden = false
            birthdate = person!.birthdate
            self.set(segmetedControl: areaSegmentedControl, selected: person!.area)
            saveButton.setTitle("Actualizar", for: .normal)
        }else{
            deleteButton.isHidden = true
            saveButton.setTitle("Guardar", for: .normal)
        }
    }
    
    func set(textField: UITextField, text: String){
        textField.layer.cornerRadius = borderRadius
        textField.layer.borderWidth = borderWith
        textField.layer.borderColor = borderColor
        textField.delegate = self
        textField.text = text
    }
    
    func set(segmetedControl: UISegmentedControl, selected: String){
        switch selected {
        case "Software":
            segmetedControl.selectedSegmentIndex = 0
            break
        case "Consultoria":
            segmetedControl.selectedSegmentIndex = 1
            break
        default:
            segmetedControl.selectedSegmentIndex = 0
            break
        }
    }
    
    func setDatePickerFor(textField: UITextField) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        textField.inputView = datePicker

        // Add toolbar with done button on the right
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 1))
        let flexibleSeparator = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneButtonPressed(_:)))
        toolbar.items = [flexibleSeparator, doneButton]
        textField.inputAccessoryView = toolbar
    }
    
    @objc func doneButtonPressed(_ sender: Any){
        self.birthdateTextField.resignFirstResponder()
    }
    
    @objc func dateChanged(_ datePicker: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        self.birthdateTextField.text = formatter.string(from: datePicker.date)
        self.birthdate = formatter.string(from: datePicker.date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func areaChanged(_ sender: Any) {
        switch areaSegmentedControl.selectedSegmentIndex
        {
        case 0:
            area = "Software"
        case 1:
            area = "Consultoria"
        default:
            break
        }
    }
    
    @IBAction func dismiss(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func save(_ sender:Any){
        
        if(isValidInput()){
            saveButton.setTitle("Guardando...", for: .normal)
            saveButton.isEnabled = false
            let db: DBHelper = DBHelper()
            let id = person?.id ?? 0
            let newPerson = Person(id: id,
                                area: area,
                                name: nameTextField.text!,
                                lastName: lastNameTextField.text!,
                                lastName2: lastName2TextField.text!,
                                phone: phoneTextField.text!,
                                birthdate: birthdate,
                                email: emailTextField.text!)
            if person == nil {
                db.insert(person: newPerson)
            }else{
                db.update(person: newPerson)
            }
            self.dismiss(animated: true, completion: {
                self.delegate?.itemSaved()
            })
        }
        
    }
    
    @IBAction func deleteItem(_ sender:Any){
        
        deleteButton.setTitle("...", for: .normal)
        deleteButton.isEnabled = false
        
        saveButton.isHidden = true
        
        if let person = person {
            let db: DBHelper = DBHelper()
            
            db.deleteByID(id: person.id)
            self.dismiss(animated: true, completion: {
                self.delegate?.itemSaved()
            })
        }
        
        
    }
    
    func isValidInput() -> Bool {
        
        if(nameTextField.text!.count == 0){
            self.nameTextField.becomeFirstResponder()
            self.showError(msg: "Escriba un nombre")
            return false
        }
        
        if lastNameTextField.text!.count == 0 {
            self.lastNameTextField.becomeFirstResponder()
            self.showError(msg: "Escriba un apellido paterno")
            return false
        }

        if lastName2TextField.text!.count == 0 {
            self.lastName2TextField.becomeFirstResponder()
            self.showError(msg: "Escriba un apellido materno")
            return false
        }
        
        if emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            self.emailTextField.becomeFirstResponder()
            self.showError(msg: "Escriba un email")
            return false
        }
        
        if !isValidEmail(email: self.emailTextField.text!) {
            self.emailTextField.becomeFirstResponder()
            self.showError(msg: "Escriba un email válido")
            return false
        }
        
        if phoneTextField.text!.count == 0 {
            self.phoneTextField.becomeFirstResponder()
            self.showError(msg: "Escriba un teléfono")
            return false
        }
        
        if birthdate.count == 0 {
            self.showError(msg: "Elija una fecha de cumpleaños")
            return false
        }
        
        return true
    }
    
    func isValidEmail( email: String) -> Bool {
        
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    func showError(msg: String){
        let alertDisapperTimeInSeconds = 1.5
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .actionSheet)
        self.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
          alert.dismiss(animated: true)
        }
    }
}
